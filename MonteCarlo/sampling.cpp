/* given sampling of uniform distribution in [0,1]
   sample distribution f(x)

   solution: solve F(y) = x => y = F^-1(x)
             sample x
             convert to y = F^-1(x)
*/

vector<float> sampleSin(int n)
{
    vector<float> result;

    for(int i = 0; i < n; i++) {
        float x = rand();
        float y = acos(x);
        result.push_back(y);
    }
}
