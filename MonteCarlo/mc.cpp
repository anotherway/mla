/* description
 monte carlo is an method to calculate integration with  importance sampling.
*/

float mc(function fx) {
    sample(g(x));
    sum(f(x)/g(x));
}
